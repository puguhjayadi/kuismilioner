const mysql = require("mysql");
const dbConfig = require("../config/config.db.js");

const connection1 = mysql.createConnection({
	host: dbConfig.database1.HOST,
	user: dbConfig.database1.USER,
	password: dbConfig.database1.PASSWORD,
	database: dbConfig.database1.DB
});

const connection2 = mysql.createConnection({
	host: dbConfig.database2.HOST,
	user: dbConfig.database2.USER,
	password: dbConfig.database2.PASSWORD,
	database: dbConfig.database2.DB
});

module.exports = {
	connection1 : connection1,
	connection2 : connection2
};

