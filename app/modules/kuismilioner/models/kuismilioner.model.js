const PORT = process.env.PORT || 3000;
const {connection1, connection2} = require("../../../core/core.connection.js");
const dbConfig = require("../../../config/config.db.js");

module.exports = function() {

	return {

		getMilipoint : function(norek, result) {

			const rawQuery = "SELECT trx.trx_balance as milipoint FROM "+dbConfig.database2.DB+".member_trx as trx WHERE trx.member_rekening_code = ? ORDER BY trx.member_trx_code DESC LIMIT 1";

			this.execRawQuery(rawQuery, [norek], result);
		},


		getRekening : function(norek, result) {

			const rawQuery = "SELECT COUNT(member_rekening_code) as norek FROM "+dbConfig.database2.DB+".member_rekening WHERE member_rekening_code = ? LIMIT 1";

			this.execRawQuery(rawQuery, [norek], result);
		},


		getEmail : function(email, result) {

			const rawQuery = "SELECT COUNT(member_login_id) as mail FROM "+dbConfig.database2.DB+".member_login_kmpay WHERE member_email = ? LIMIT 1";

			this.execRawQuery(rawQuery, [email], result);
		},


		execRawQuery : function(rawQuery, params = '', result){
			connection1.query(rawQuery, params, (err, res) => {
				if (err) {
					console.log("error: ", err);
					result(err, null);
					return;
				}
				result(null, res);
			});
		},

		store : function (object, result) {
			let login  = {
				member_password : object.password,
				member_email : object.email
			}

			// member_login_kmpay
			connection1.query("INSERT IGNORE INTO "+dbConfig.database2.DB+".member_login_kmpay SET ?", login, (err, res_member_login) => {

				if (err) {
					console.log("error: ", err);
					result(err, null);
					return;
				}

				let member = { 
					member_login_id : res_member_login.insertId,
					fullname : object.fullname,
					cellular_number : object.cellular_number
				}

				// member
				connection1.query("INSERT IGNORE INTO "+dbConfig.database1.DB+".member SET ?", member, (err, res_member) => {

					// member_rekening
					let member_rekening = { 
						member_id : res_member.insertId,
						member_rekening_code : object.norek
					}

					connection1.query("INSERT IGNORE INTO "+dbConfig.database2.DB+".member_rekening SET ?", member_rekening);


					// sosial_media
					let sosial_media = { 
						member_id : res_member.insertId,
						sosmed_type : 'KMPAY',
						sosmed_account_id : object.norek
					}

					connection1.query("INSERT IGNORE INTO "+dbConfig.database1.DB+".sosial_media SET ?", sosial_media);

				});	

				result(null, object.norek);

			});
		},
	}
};




