const kuismilioner  = require("../models/kuismilioner.model.js")();
const moment= require( 'moment' );


// http://localhost:3000/kuismilioner/100101

exports.show = (req, res) => {

	let norek = req.params.norek;

	if (!norek) {
		res.status(400).json({ 
			status: 400,
			message: 'Account number is required, please register first'
		});
		return;
	}

	kuismilioner.getRekening(norek, (err, check) => {
		if(check[0].norek < 1){
			res.status(400).json({ 
				status : 400,
				message: 'Not found, must registered'
			});
			return;

		} else {
			kuismilioner.getMilipoint(norek, (err, data) => {
				if (err){
					res.status(500).send({
						message:
						err.message || "Some error"
					});
				} else { 
					res.status(200).json({
						status: 200,
						message: 'Data found with account number : '+norek,
						data: data
					})
				}
			});

		}
	});
};

exports.store = (req, res) => {
	if (!req.body) {
		res.status(400).send({
			message: "Content can not be empty!"
		});
	}

	let object = req.body;
	object.password = 123;
	object.norek = Math.floor(100000 + Math.random() * 900000);

	kuismilioner.getEmail(req.body.email, (err, data) => {

		if( data[0].mail < 1){

			kuismilioner.store(req.body, (err, data) => {
				if (err){
					res.status(500).send({
						message:
						err.message || "Some error "
					});
				} else {
					res.status(200).json({
						status: 200,
						message: 'Segister success',
						data: 'norek : '+data
					})
				}
			});

		} else {
			res.status(409).json({ 
				status: 409,
				message: 'Email already registered'
			});
		}
	});
};

