const kuismilioner = require("../controllers/kuismilioner.controller.js");

module.exports = app => {
    app.get("/kuismilioner", kuismilioner.show);
    app.get("/kuismilioner/:norek", kuismilioner.show);
    app.post("/kuismilioner", kuismilioner.store);
    app.post("/kuismilioner/store", kuismilioner.store);

};