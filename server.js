const express = require("express");
const bodyParser = require("body-parser");
const path = require('path');
const url = require('url');

const app = express();

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

require("./app/config/config.router.js")(app);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});

